#define PRESSED 0
#define RELEASED 1
/* 
 *  Map each input on the card to the keycode it should generate.
 *  
 *  2 -> 91 (å)
 *  3 -> 39 (ä)
 *  4 -> 59 (ö)
 */
const int buttons[] = {
  false,  
  false,
  91,
  39,
  59
};

/* 
 * To avoid false button presses, we wait a while between each 
 * press/release.
 */ 
const int bounceThreshold = 100;

/* 
 * Add two arrays to keep track of if a buttons is beeing pressed
 * and if it is in bouncing.
 */ 
int pressed[sizeof(buttons)/sizeof(int)];
long bounces[sizeof(buttons)/sizeof(int)];

void setup()
{
  /* 
   * Set up all pins. Since the two first items in the buttons 
   * array are empty, we begin from item two.
   */
  for (int i = 2; i < sizeof(buttons); i++) {
      pinMode(i, INPUT);
      digitalWrite(i, HIGH);
  }
  
  Keyboard.begin();
}

void loop()
{
  for (int i = 2; i < sizeof(buttons)/sizeof(int); i++) {

    // If this button is very recently pressed/released, continue
    if (millis() - bounces[i] < bounceThreshold) {
      continue;
    }

    // If the button is released and it's currently pressed, 
    // release it
    if (digitalRead(i) == RELEASED && pressed[i]) {
      Keyboard.release(buttons[i]);
      
      pressed[i] = false;
      bounces[i] = millis();
    }

    // If the button is pressed and it wasn't previously, press it
    if (digitalRead(i) == PRESSED && !pressed[i]) {
      Keyboard.press(buttons[i]);
      
      pressed[i] = true;
      bounces[i] = millis();
    } 
  }
}
