# Keyboard

This Arduino project is an extra keyboard, defaulting to three keys mapping to the Swedish å, ä and ö characters.

### Parts
  - 1 * Arduino Leonardo compatible board
  - x * Switches (one per key)
  
### Schematics
![alt text](keyboard_bb.png "Schematics")

### Extending
To implement a custom keyboard, connect each button to a pin from 2 and up and to ground. Then bring up keyboard.ino and add the keycode that the button should generate to the *buttons* array. Add the keycode for pin 2 to index 2 in the array, the keycode for pin 3 to index 3 and so on. Keep in mind that the array is 0 indexed and therefore the first two elements will be sentinel values. 

